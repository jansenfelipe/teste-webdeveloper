<?php

$factory->define(App\Importation::class, function () {
    return [
        'path' => base_path('tests') . DIRECTORY_SEPARATOR . 'products_teste_webdev_leroy.xlsx',
        'status' => 'Processando',
        'detalhe' => null
    ];
});
