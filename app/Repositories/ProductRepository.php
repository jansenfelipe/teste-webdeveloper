<?php

namespace App\Repositories;


use App\Product;

class ProductRepository
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ProductRepository constructor.
     * @param $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Save product
     *
     * @param array $params
     */
    public function save(array $params)
    {
        $product = $this->product->where('lm', $params['lm'])->first();

        if(is_null($product))
            $product = new Product();

        $product->fill($params);
        $product->save();
    }
}