<?php

namespace App\Http\Controllers;

use App\Importation;
use App\Jobs\ImportProducts;
use App\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var Product
     */
    private $productModel;

    /**
     * @var Importation
     */
    private $importationModel;

    /**
     * ProductController constructor.
     *
     * @param Product $product
     * @param Importation $importation
     */
    public function __construct(Product $product, Importation $importation)
    {
        $this->productModel = $product;
        $this->importationModel = $importation;
    }

    /**
     * List products
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $products = $this->productModel->paginate(10);
        $importations = $this->importationModel->orderBy('created_at', 'DESC')->get();

        return view('products.index', compact('products', 'importations'));
    }

    /**
     * Upload file
     *
     * @param Request $request
     * @param Importation $importationModel
     */
    public function postImportar(Request $request, Importation $importationModel)
    {
        //Validation
        $this->validate($request, [
           'file' => 'file|mimes:xls,xlsx'
        ]);

        $filename = date('YmdHis') . '_' . $request->file('file')->getClientOriginalName();

        //Moving file
        $request->file('file')->move(storage_path('app'), $filename);

        $importation = $importationModel->create([
            'path' => storage_path('app') . DIRECTORY_SEPARATOR . $filename,
        ]);

        $this->dispatch(new ImportProducts($importation));

        return redirect()->back()->with(['info' => 'Upload realizado com sucesso! A importação está sendo processada']);
    }

    /**
     * Detail product
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForm($id)
    {
        $product = $this->productModel->findOrFail($id);

        return view('products.form', compact('product'));
    }

    /**
     * Save product
     */
    public function postSave(ProductRepository $productRepository, Request $request)
    {
        $productRepository->save($request->all());

        return redirect('/')->with(['info' => 'Produto salvo com sucesso!']);
    }

    /**
     * Delete product
     */
    public function getDelete($id)
    {
        $product = $this->productModel->findOrFail($id);

        $product->delete();

        return redirect('/')->with(['info' => 'Produto deletado com sucesso!']);
    }
}
