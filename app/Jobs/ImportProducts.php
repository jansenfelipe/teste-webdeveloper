<?php

namespace App\Jobs;

use App\Importation;
use App\Repositories\ProductRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use PHPExcel_IOFactory;

class ImportProducts extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $importation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Importation $importation)
    {
        $this->importation = $importation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ProductRepository $productRepository)
    {
        $reader = PHPExcel_IOFactory::createReaderForFile($this->importation->path);
        $reader->setReadDataOnly();
        $reader->setLoadSheetsOnly(['Plan1']);

        $phpExcel = $reader->load($this->importation->path);

        $i = 0;
        $categoria = null;

        foreach ($phpExcel->getActiveSheet()->toArray() as $row)
        {
            if($i == 1)
                $categoria = $row[1];

            if($i >= 4)
            {
                $productRepository->save([
                    'lm' => $row[0],
                    'name' => $row[1],
                    'free_shipping' => $row[2],
                    'description' => $row[3],
                    'price' => $row[4],
                    'category' => $categoria
                ]);
            }

            $i++;
        }

        $this->importation->update(['status' => 'Processado']);
    }
}
