@extends('app')

@section('content')
    <h3>Gerenciamento de produtos</h3>

    <hr />

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Produtos</a></li>
        <li role="presentation"><a href="#importations" aria-controls="importations" role="tab" data-toggle="tab">Importações</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="products">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>LM</th>
                    <th>Category</th>
                    <th>Name</th>
                    <th>Free shipping</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($products as $product)
                    <tr>
                        <th>#{{ $product->lm }}</th>
                        <td>{{ $product->category }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->free_shipping }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->price }}</td>
                        <td>
                            <a href="{{ action('ProductController@getForm', $product->id) }}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="{{ action('ProductController@getDelete', $product->id) }}" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">Nenhum produto encontrado</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

            {!! $products->render() !!}

        </div>
        <div role="tabpanel" class="tab-pane" id="importations">

            <form action="/importar" method="POST" enctype="multipart/form-data">
                <input type="file" name="file" id="file" style="display: none;" />
                {{ csrf_field() }}
                <button type="button" id="importar" class="btn btn-success"><i class="glyphicon glyphicon-upload"></i> Importar .xls</button>
            </form>


            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Path</th>
                    <th>Status</th>
                    <th>Data</th>
                </tr>
                </thead>
                <tbody>
                @forelse($importations as $importation)
                    <tr>
                        <td>{{ $importation->path }}</td>
                        <td>{{ $importation->status }}</td>
                        <td>{{ $importation->created_at }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3">Nenhuma importação realizada</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>



    <script>
        $(function(){
            $('#importar').click(function(){
                $('#file').trigger('click');
            });

            $('#file').change(function(){
                this.form.submit();
            });
        });
    </script>
@endsection