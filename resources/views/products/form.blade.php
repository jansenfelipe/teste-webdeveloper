@extends('app')

@section('content')
    <h3>Editar: #{{ $product->lm }} - {{ $product->name }}</h3>

    <hr />

    <form action="{{ action('ProductController@postSave') }}" method="POST">
        {{ csrf_field() }}

        <div class="form-group">
            <label>Category</label>
            <input type="text" class="form-control" name="category" value="{{ old('category', $product->category) }}" />
        </div>

        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{ old('name', $product->name) }}" />
        </div>

        <div class="form-group">
            <label>Free Shipping</label>
            <input type="text" class="form-control" name="free_shipping" value="{{ old('free_shipping', $product->free_shipping) }}" />
        </div>

        <div class="form-group">
            <label>Description</label>
            <input type="text" class="form-control" name="description" value="{{ old('description', $product->description) }}" />
        </div>

        <div class="form-group">
            <label>Price</label>
            <input type="text" class="form-control" name="price" value="{{ old('price', $product->price) }}" />
        </div>

        <input type="hidden" name="lm" value="{{ $product->lm }}" />

        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="/" class="btn btn-default">Voltar</a>
    </form>
@endsection