# Gerenciamento de Produtos

Aplicação web de gerenciamento de produtos que permite importar arquivo .xls para criar produtos.

## Como usar

Clone este repositório:

```ssh
$ git clone https://jansenfelipe@bitbucket.org/jansenfelipe/teste-webdeveloper.git
```

Acesse o diretório `teste-webdeveloper` e instale as dependências:

```ssh
$ cd teste-webdeveloper
$ composer install
```

Crie o arquivo `.env` e configure as variáveis

```ssh
$ cp .env.example .env;
$ vim .env
```

Crie a estrutura do banco de dados

```ssh
$ php artisan migrate
```

Inicie o servidor

```ssh
$ php artisan serve
```