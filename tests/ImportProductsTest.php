<?php

use App\Jobs\ImportProducts;
use App\Repositories\ProductRepository;

class ImportProductsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHandle()
    {
        $importation = factory(App\Importation::class)->make();

        $productRepositoryMock = Mockery::mock(ProductRepository::class);
        $productRepositoryMock->shouldReceive('save')->times(6);

        $importProducts = new ImportProducts($importation);
        $importProducts->handle($productRepositoryMock);
    }
}
